{
  name: "GEO",
  index: "leadblock"
  type: "box",
  size: ["10*cm","10*cm","10*cm"]
  position: ["0.0*m","0.0*m","0.0*m"]
  rotation: [0.0, 0.0, 0.0]
  material: "G4_Pb"
  mother: "world"
}

